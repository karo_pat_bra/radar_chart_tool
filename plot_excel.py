import plotly.graph_objects as go
import plotly.express as px
import openpyxl
import numpy as np
import pandas as pd

class RadarChartToExcel:
    def __init__(self, source):
        """
        Initializes the RadarChartToExcel class.

        Parameters:
            data (dict): A dictionary containing data for the radar chart.
                         The dictionary should have category names as keys and
                         corresponding values for each category as values.
            filename (str): The name of the Excel file to store the chart.
        """
        self.sourcefile = source
       

    def create_radar_chart(self):
        """
        Creates a radar chart with the provided data.
        """
        
        df = self.read_csv(self.sourcefile)
        df = df.transpose()
        categories = df.columns
    
        max = df.max()
        fig = go.Figure()

        for index in df.index:
        
            temptlst = df.loc[index].values
            temptlst=temptlst.tolist()
            temptlst.append(temptlst[0])
            catlist = categories.tolist()
            catlist.append(categories[0])
            values = temptlst
            fig.add_trace(go.Scatterpolar(r=values,theta=catlist,name=index))

        fig.update_layout(polar=dict(radialaxis=dict(visible=True,range=[0, max],)),showlegend=True)   
        fig.show()



    def read_csv(self, csv_file_path):
        """
        Reads a CSV file and creates a DataFrame.

        Parameters:
            csv_file_path (str): The path to the CSV file.

        Returns:
            pandas.DataFrame: The DataFrame containing the data from the CSV file.
        """
       
            # Read the CSV file into a DataFrame
        dataframe = pd.read_csv(csv_file_path)
        return dataframe
    
    
        



